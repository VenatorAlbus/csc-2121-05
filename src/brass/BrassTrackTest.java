package brass;

import org.junit.*;
import static org.junit.Assert.*;

public class BrassTrackTest
{
	private BrassTrack bt;
	private int allowed_error;
	
	//DO THIS
	//declare a BrassCottonDemandTrack object
	BrassCottonDemandTrack bcd;
	
	
	@Before 
	public void setUp() 
	{
		BrassXML bx = new BrassXML("resources/brass_pixels.xml");
		bt = new BrassTrack(bx);
		allowed_error = 2;  //+- two pixels is the allowed error
		
		//DO THIS (do the cotton demand track part last)
		//create a BrassCottonDemandTrack object
		bcd = new BrassCottonDemandTrack(bx);
		
    }
	
	
	
	@Test
	public void brassCottonDemand() {
		//moves the marker down and returns extra income
		int cotton_demand_extra_income = bcd.cottonTrackIncome();   // 3, 2, 1, 0 , -1
		//make sure marker is in correct location
		int cotton_demand_index = bcd.getCottonDemandIndex();  // returns 0-8 
		
		//asserts
		assertTrue("Brass Cotton Demand Income 1'st: WRONG", cotton_demand_extra_income == 3);
		assertTrue("Brass Cotton Demand Index 1'st: WRONG", cotton_demand_index == 2);
		
		cotton_demand_extra_income = bcd.cottonTrackIncome();
		cotton_demand_index = bcd.getCottonDemandIndex();
		
		assertTrue("Brass Cotton Demand Income 2'st: WRONG", cotton_demand_extra_income == 3);
		assertTrue("Brass Cotton Demand Index 2'st: WRONG", cotton_demand_index == 3);
	}
	
	@Test
	public void brassLoan() {
		int loan = bt.takeLoan(10, 20);
		assertTrue("Brass Loan: WRONG", loan == 8);

		loan = bt.takeLoan(30, 10);
		assertTrue("Brass Loan: WRONG", loan == 28);
	}

	@Test
	public void brassIncome() {
		int income = bt.getIncomeAmount(0);
		assertTrue("Brass Income: WRONG", income == -10);

		income = bt.getIncomeAmount(1);
		assertTrue("Brass Income: WRONG", income == -9);

		income = bt.getIncomeAmount(2);
		assertTrue("Brass Income: WRONG", income == -8);

		income = bt.getIncomeAmount(10);
		assertTrue("Brass Income: WRONG", income == 0);

		income = bt.getIncomeAmount(11);
		assertTrue("Brass Income: WRONG", income == 1);

		income = bt.getIncomeAmount(12);
		assertTrue("Brass Income: WRONG", income == 1);

		income = bt.getIncomeAmount(30);
		assertTrue("Brass Income: WRONG", income == 10);
	}
	
	//(x, y) for location 0 is specified in brass_pixels.xml
	//as it doesn't depend on the offsets, this test should certainly pass
	@Test
	public void brassLocTrackTest0()
	{
		int x_pos = bt.getXPixel(0);
		int y_pos = bt.getYPixel(0);
		assertTrue("Brass Track Loc X 0: TOO LOW", 322 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 322 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 642 >= (y_pos - allowed_error));
		
		
		
	}
	
	@Test
	public void brassLocTrackTest1()
	{
		int x_pos = bt.getXPixel(1);
		int y_pos = bt.getYPixel(1);
		assertTrue("Brass Track Loc X 0: TOO LOW", 302 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 302 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 642 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest2()
	{
		int x_pos = bt.getXPixel(5);
		int y_pos = bt.getYPixel(5);
		assertTrue("Brass Track Loc X 0: TOO LOW", 226 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 226 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 642 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest3()
	{
		int x_pos = bt.getXPixel(10);
		int y_pos = bt.getYPixel(10);
		assertTrue("Brass Track Loc X 0: TOO LOW", 132 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 132 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 642 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest4()
	{
		int x_pos = bt.getXPixel(16);
		int y_pos = bt.getYPixel(16);
		assertTrue("Brass Track Loc X 0: TOO LOW", 20 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 20 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 642 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 642 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest5()
	{
		int x_pos = bt.getXPixel(17);
		int y_pos = bt.getYPixel(17);
		assertTrue("Brass Track Loc X 0: TOO LOW", 20 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 20 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 623 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 623 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest6()
	{
		int x_pos = bt.getXPixel(18);
		int y_pos = bt.getYPixel(18);
		assertTrue("Brass Track Loc X 0: TOO LOW", 20 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 20 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 605 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 605 >= (y_pos - allowed_error));
		
	}


	//(x, y) for location 55 is (151, 494)
	//(x, y) for location 64 is (19, 458)
	//(x, y) for location 70 is (94, 421)
	//(x, y) for location 76 is (19, 386)
	//(x, y) for location 82 is (94, 349)
	//(x, y) for location 88 is (18, 311)
	//(x, y) for location 93 is (74, 275)
	//(x, y) for location 98 is (18, 238)
	@Test
	public void brassLocTrackTest7()
	{
		int x_pos = bt.getXPixel(32);
		int y_pos = bt.getYPixel(32);
		assertTrue("Brass Track Loc X 0: TOO LOW", 245 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 245 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 568 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 568 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest8()
	{
		int x_pos = bt.getXPixel(46);
		int y_pos = bt.getYPixel(46);
		assertTrue("Brass Track Loc X 0: TOO LOW", 19 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 19 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 532 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 532 >= (y_pos - allowed_error));
		
	}
	

	@Test
	public void brassLocTrackTest9()
	{
		int x_pos = bt.getXPixel(55);
		int y_pos = bt.getYPixel(55);
		assertTrue("Brass Track Loc X 0: TOO LOW", 151 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 151 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 494 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 494 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest10()
	{
		int x_pos = bt.getXPixel(64);
		int y_pos = bt.getYPixel(64);
		assertTrue("Brass Track Loc X 0: TOO LOW", 19 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 19 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 458 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 458 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest11()
	{
		int x_pos = bt.getXPixel(70);
		int y_pos = bt.getYPixel(70);
		assertTrue("Brass Track Loc X 0: TOO LOW", 94 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 94 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 421 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 421 >= (y_pos - allowed_error));
		
	}

	@Test
	public void brassLocTrackTest12()
	{
		int x_pos = bt.getXPixel(76);
		int y_pos = bt.getYPixel(76);
		assertTrue("Brass Track Loc X 0: TOO LOW", 19 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 19 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 386 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 386 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest13()
	{
		int x_pos = bt.getXPixel(82);
		int y_pos = bt.getYPixel(82);
		assertTrue("Brass Track Loc X 0: TOO LOW", 94 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 94 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 349 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 349 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest14()
	{
		int x_pos = bt.getXPixel(88);
		int y_pos = bt.getYPixel(88);
		assertTrue("Brass Track Loc X 0: TOO LOW", 18 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 18 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 311 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 311 >= (y_pos - allowed_error));
		
	}


	//(x, y) for location 98 is (18, 238)
	@Test
	public void brassLocTrackTest15()
	{
		int x_pos = bt.getXPixel(93);
		int y_pos = bt.getYPixel(93);
		assertTrue("Brass Track Loc X 0: TOO LOW", 74 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 74 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 275 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 275 >= (y_pos - allowed_error));
		
	}
	
	@Test
	public void brassLocTrackTest16()
	{
		int x_pos = bt.getXPixel(98);
		int y_pos = bt.getYPixel(98);
		assertTrue("Brass Track Loc X 0: TOO LOW", 18 <= (x_pos + allowed_error));
		assertTrue("Brass Track Loc X 0: TOO HIGH", 18 >= (x_pos - allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO LOW", 238 <= (y_pos + allowed_error));
		assertTrue("Brass Track Loc Y 0: TOO HIGH", 238 >= (y_pos - allowed_error));
		
	}
	//(x, y) for location 1 is (302, 642)
	//(x, y) for location 5 is (226, 642)
	//(x, y) for location 10 is (132, 642)
	//(x, y) for location 16 is (20, 642)
	//(x, y) for location 17 is (20, 623)
	//(x, y) for location 18 is (20, 605)
	//(x, y) for location 32 is (245, 568)
	//(x, y) for location 46 is (19, 532)
	//(x, y) for location 55 is (151, 494)
	//(x, y) for location 64 is (19, 458)
	//(x, y) for location 70 is (94, 421)
	//(x, y) for location 76 is (19, 386)
	//(x, y) for location 82 is (94, 349)
	//(x, y) for location 88 is (18, 311)
	//(x, y) for location 93 is (74, 275)
	//(x, y) for location 98 is (18, 238)
}
